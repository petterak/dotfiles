# _               _              
#| |__   __ _ ___| |__  _ __ ___ 
#| '_ \ / _` / __| '_ \| '__/ __|
#| |_) | (_| \__ \ | | | | | (__ 
#|_.__/ \__,_|___/_| |_|_|  \___|
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
# bakcup PS1='[\$BASH \W]$ '

# Enable vi-mode
set -o vi
# ..while keeping ctrl+l as clear screen
bind -m vi-insert "\C-l":clear-screen

# autocd
shopt -s autocd

# Fix line wrap
shopt -s checkwinsize

### Aliases ###
alias ll='ls -lh'
alias lla='ls -lah'
# youtube viewer and downloader
alias yv='youtube-viewer'
alias yvm='youtube-viewer -n'
alias yd='youtube-dl'
alias ydm='youtube-dl -x --audio-format mp3'
# Windscribe VPN
alias ws='windscribe'
alias wsc='windscribe connect'
alias wsd='windscribe disconnect'
# dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
# ranger use current folder on exit
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
